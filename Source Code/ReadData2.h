/**
 *   Easy toolbox for reading characters and numbers.
 * 
 *
 *   @file     readData2.H
 *   @author   Frode Haug, NTNU
 *   @translator J�rgen Teig
 */


#ifndef __READDATA2_H
#define __READDATA2_H


#include <iostream>            //  cin, cout
#include <iomanip>             //  setprecision
#include <cctype>              //  toupper
#include <cstdlib>             //  atoi, atof


const int  MAXCHAR = 200;      //  Maximum characters in input buffer.


char  readChar(const char* t);
int   readInt(const char* t, const int min, const int max);


/**
 *  Reads and returns an uppercased character.
 *
 *  @param   t  - Prompt for the user when requesting a character.
 *
 *  @return  An uppercased character.
 */
 char readChar(const char* t)  {
     char letter;
     std::cout << t << ":  ";
     std::cin >> letter;  std::cin.ignore(MAXCHAR, '\n');
     return (toupper(letter));
}


/**
 *  Reads and returns an integer between two given limits.
 *
 *  @param   t    - Prompt for the user when requesting input / a number.
 *  @param   min  - Minimum for read and accepted numerical value.
 *  @param   max  - Maximum for a read and accepted numerical value.
 *
 *  @return  Accepted value in the interval 'min' - 'max'
 */
int readInt(const char* t, const int min, const int max)  {
    char buffer[MAXCHAR] = "";
    int  number = 0;
    bool error = false;

    do {
        error = false;
        std::cout << t << " (" << min << " - " << max << "):  ";
        std::cin.getline(buffer, MAXCHAR);
        number = atoi(buffer);
        if (number == 0 && buffer[0] != '0') {
            error = true;   std::cout << "\nERROR: Not an integer\n\n";
        }
    } while (error  ||  number < min  ||  number > max);

    return number;
}


#endif
