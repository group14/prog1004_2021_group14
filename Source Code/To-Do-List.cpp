/**
 *  To Do List
 *
 *  This program does the following:
 *  - Lets the user add tasks.
 *  - Lets the user enter name, description, category, start date, end date, deadline and priority.
 *  - Lets the user reorder his/her added tasks.
 *  - Lets the user change status of tasks to finished.
 *
 *  @author Mats Jensen, Jorgen Teig
 *  @file To-Do-List.cpp
 */


#include <iostream>                 // Cout, cin
#include <ctime>                    // Time
#include <iomanip>                  // Setw
#include <fstream>                  // Ifstream, ofstream
#include <string>                   // String
#include <vector>                   // Vector
#include <list>                     // List
#include <algorithm>                // Find
#include "ReadData2.h"              // Toolcase for input of data
using namespace std;


enum Priority { low = 3, medium = 2, high = 1 };        ///< Enum for choosing priority.
enum setStatus { Done, NotDone };                       ///< Enum for marking a task as finished or marking a task as unfinished.


/**
 *  Task object (With start date (day, month year), finish date (day, month year), name, description, category, priority and status).
 */
class Task {
private:
    int startDay, startMonth, startYear,
        finishDay, finishMonth, finishYear;
    string name,
        description,
        category;
    Priority priority;
    bool status;

public:
    Task();
    Task(string taskName);
    Task(ifstream& in);
    bool equalName(string taskName);
    string getName();
    void changeStatus(enum setStatus s);
    void writeData();
    void readData();
    void changeData();
    void writeToFile(ofstream& out);
    string getCategory();
    int getStartDate();
    int getFinishDate();
    Priority getPriority();
    bool getStatus();
};


//      FUNCTION DECLARATION       //


void changeTask();
void deleteTask();
void changeStatus();
void newTask();
void readFromFile();
void showTask();
void showAllTasks();
void showTasksWithPriority();
void writeMenu();
void writeChangeMenu();
void writeToFile();
void sortTasks();
void sortMenu();
void moveTask();
void enterToContinue();


list <Task*> gTasks;              ///< Datastructure with all task objects.


/**
 *  Main program.
 */
int main() {
    char command;

    readFromFile();

    writeMenu();

    command = readChar("\tCommand");

    while (command != 'Q') {
        switch (command) {
        case 'N': newTask();                    writeMenu();    break;
        case 'P': showTask();                   writeMenu();    break;
        case 'S': showAllTasks();               writeMenu();    break;
        case 'O': sortTasks();                  writeMenu();    break;
        case 'R': showTasksWithPriority();      writeMenu();    break;
        case 'V': moveTask();                   writeMenu();    break;
        case 'C': changeTask();                 writeMenu();    break;
        case 'D': deleteTask();                 writeMenu();    break;
        case 'M': changeStatus();               writeMenu();    break;
        default: writeMenu();                  writeMenu();    break;
        }

        command = readChar("\tCommand");
    }

    writeToFile();

    return 0;
}


//*****************************************************************************
//*******************           Task funtions        **************************
//*****************************************************************************


/**
 *  Constructor for making a new Task object.
 */
Task::Task() {
    description = "";
    priority = low;
    status = false;
}


/**
 *  Parameter filled constructor, that sets the name with the paramater.
 *
 *  @param taskName - Sets the objects task name to taskName.
 */
Task::Task(string taskName) {
    name = taskName;
    startDay = startMonth = startYear = finishDay = finishMonth = finishYear = 0;
    description = category = "";
    status = false;
}


/**
 *  Constructor for reading task data from file.
 *
 *  @param in - File to be read from.
 */
Task::Task(ifstream& in) {
    char done;
    int prio;

    getline(in, name);

    getline(in, description);

    getline(in, category);

    in >> startDay >> startMonth >> startYear;
    in.ignore();

    in >> finishDay >> finishMonth >> finishYear;
    in.ignore();

    in >> prio;
    priority = static_cast<Priority>(prio);
    in.ignore();

    in >> done;
    if (done == '+')
        status = true;
    else
        status = false;
    in.ignore();
}


/**
 *  Returns true if name is equal to taskName, otherwise returns false.
 *
 *  @return True if taskName is equal to name in the task object.
 */
bool Task::equalName(string taskName) {
    return (name == taskName);
}


/**
 *  Returns the name of the task.
 *
 *  @return Name of the task object.
 */
string Task::getName() {
    return name;
}


/**
 *  Marks the task as done by setting status to true.
 */
void Task::changeStatus(enum setStatus s) {
    switch (s) {
    case Done:    status = true; break;
    case NotDone: status = false; break;
    }
}


/**
 *  Writes the data in the task object.
 */
void Task::writeData() {

    cout << "Name:             " << name << "\n";
    cout << "\tDescription:      " << description << "\n";
    cout << "\tCategory:         " << category << "\n";
    cout << "\tStart date:       " << startDay << "/" << startMonth << "/" << startYear << "\n";
    cout << "\tFinish date:      " << finishDay << "/" << finishMonth << "/" << finishYear << "\n";
    cout << "\tCurrent priority: ";

    switch (priority) {
    case low:    cout << "Low\n";    break;
    case medium: cout << "Medium\n"; break;
    case high:   cout << "High\n";   break;
    }

    cout << "\tStatus:           " << (status ? "Finished" : "Unfinished") << "\n";
}


/**
 *  Changes data in the task object.
 *
 *  @see Task::writeChangeMenu().
 *  @see readChar(...).
 */
void Task::changeData() {
    char command, prio;

    writeChangeMenu();
    command = readChar("\tCommand");

    while (command != 'Q') {
        switch (command) {
        case 'N': {
            cout << "\n\tWhat is the new task's name: ";
            getline(cin, name);
            break;
        }
        case 'D': {
            cout << "\n\tWhat is the new task's description: ";
            getline(cin, description);
            break;
        }
        case 'C': {
            cout << "\n\tWhat is the new task's category: ";
            getline(cin, category);
            break;
        }
        case 'S': {
            startDay = readInt("What is the new task's start day", 1, 31);
            startMonth = readInt("What is the new task's start month", 1, 12);
            startYear = readInt("What is the new task's start year", 1900, 2100);
            break;
        }
        case 'F': {
            finishDay = readInt("What is the new task's finish day", 1, 31);
            finishMonth = readInt("What is the new task's finish month", 1, 12);
            finishYear = readInt("What is the new task's finish year", 1900, 2100);
            break;
        }
        case 'P': {

            do {
                prio = readChar("\tWhat is the new task's priority (L)ow, (M)edium, (H)igh: ");
            } while (prio != 'L' && prio != 'M' && prio != 'H');

            switch (prio) {
            case 'L': priority = low;    break;
            case 'M': priority = medium; break;
            case 'H': priority = high;   break;
            }

            break;
        }
        default:   writeChangeMenu();     break;
        }

        writeChangeMenu();

        command = readChar("\tCommand");
    }
}


/**
 *  Reads the input from user and sets the task objects data.
 *
 *  @see readInt(...).
 */
void Task::readData() {
    char prio;

    cout << "\n\tWhat is the tasks description: ";
    getline(cin, description);

    cout << "\n\tWhat is the tasks category: ";
    getline(cin, category);
    cout << "\n";

    startDay = readInt("\tWhat is the new task's start day", 1, 31);
    startMonth = readInt("\tWhat is the new task's start month", 1, 12);
    startYear = readInt("\tWhat is the new task's start year", 1900, 2100);
    cout << "\n";

    finishDay = readInt("\tWhat is the new task's finish day", 1, 31);
    finishMonth = readInt("\tWhat is the new task's finish month", 1, 12);
    finishYear = readInt("\tWhat is the new task's finish year", 1900, 2100);

    do {
        prio = readChar("\tWhat is the tasks priority (L)ow, (M)edium, (H)igh: ");
    } while (prio != 'L' && prio != 'M' && prio != 'H');

    switch (prio) {
    case 'L': priority = low;    break;
    case 'M': priority = medium; break;
    case 'H': priority = high;   break;
    }

    // If status is false it is not completed.
    status = false;
}


/**
 *  Writes a the task objects data to file.
 *
 *  @param out - Output file for the task objects data.
 */
void Task::writeToFile(ofstream& out) {
    out << name << '\n'
        << description << '\n'
        << category << '\n'
        << startDay << " " << startMonth << " " << startYear << '\n'
        << finishDay << " " << finishMonth << " " << finishYear << '\n';
    switch (priority) {
    case low:    out << "3\n"; break;
    case medium: out << "2\n"; break;
    case high:   out << "1\n"; break;
    }
    out << (status ? '+' : '-') << '\n';
}


/**
 *  Returns the category of the task.
 *
 *  @return Category of the task object
 */
string Task::getCategory() {
    return category;
}


/**
 *  Returns the start date of the task.
 *
 *  @return Returns the date as an integer added together in string form (From dd/mm/yyyy as separate integers  to yyyymmdd as single int)
 *  @see to_string(...).
 *  @see stoi(...).
 */
int Task::getStartDate() {
    string temp;

    temp += to_string(startYear);
    temp += to_string(startMonth);
    temp += to_string(startDay);

    return (stoi(temp));
}


/**
 *  Returns the finish date of the task.
 *
 *  @return Returns the date as an integer added together in string form (From dd/mm/yyyy as separate integers  to yyyymmdd as single int)
 *  @see to_string(...).
 *  @see stoi(...).
 */
int Task::getFinishDate() {
    string temp;

    temp += to_string(finishYear);
    temp += to_string(finishMonth);
    temp += to_string(finishDay);

    return (stoi(temp));
}


/**
 *  Returns the priority of the task.
 *
 *  @return The priority of the task object.
 */
Priority Task::getPriority() {
    return priority;
}


/**
 *  Returns the status of the task.
 *
 *  @return The status of the task object.
 */
bool Task::getStatus() {
    return status;
}


//*****************************************************************************
//*******************          Main Functions      ****************************
//*****************************************************************************


/**
 *  Changes the data of the entered task.
 *
 *  @see Task::writeData().
 *  @see Task::changeData().
 *  @see entertoContinue().
 */
void changeTask() {
    int counter = 1;
    string name;

    if (!gTasks.empty()) {
        cout << "Showing tasks that you can choose to change:\n";

        for (const auto& val : gTasks) {
            cout << "Task " << counter++ << ": ";
            val->writeData();
            cout << "\n";
        }

        cout << "Write the name of the task you want to change (Enter nothing to cancel):";
        getline(cin, name);

        if (!name.empty()) {
            for (const auto& val : gTasks) {
                if (val->getName() == name)
                    val->changeData();
            }
        }
        else {
            cout << "\tAborting change to task\n";
            enterToContinue();
        }
    }
    else {
        cout << "\tNo tasks in the list!\n";
        enterToContinue();
    }
}


/**
 *  Deletes the entered task.
 *
 *  @see Task::writeData().
 *  @see find_if(...).
 *  @see readChar(...).
 *  @see enterToContinue().
 */
void deleteTask() {
    int counter = 1;
    char confirm;
    string name;

    if (!gTasks.empty()) {
        cout << "\n\tShowing tasks that you can choose to delete:\n";

        for (const auto& val : gTasks) {
            cout << "\nTask " << counter++ << ":\n";
            val->writeData();
            cout << "\n";
        }

        cout << "\tWrite the name of the task you want to delete (Enter nothing to cancel): ";
        getline(cin, name);
        cout << "\n";

        if (!name.empty()) {

            auto it = find_if(gTasks.begin(), gTasks.end(),
                [name](const auto& val) { return(val->getName() == name); });

            if (*it && it != gTasks.end()) {

                do {
                    confirm = readChar("\tAre you sure you want to delete the task (Y/n)");
                } while (confirm != 'Y' && confirm != 'N');

                if (confirm == 'Y') {
                    gTasks.erase(it);
                    cout << "\n\tTask deleted!\n";
                    enterToContinue();
                }
                else {
                    cout << "\n\tAborted the deletion\n";
                    enterToContinue();
                }
            }
            else {
                cout << "\n\tNo task matching that name!\n";
                enterToContinue();
            }
        }
        else {
            cout << "\n\tAborting deletion of the task\n";
            enterToContinue();
        }
    }
    else {
        cout << "\n\tNo tasks in the list to delete!\n";
        enterToContinue();
    }
}


/**
 *  Mark a specified task as done.
 *
 *  @see Task::writeData().
 *  @see Task::equalName(...).
 *  @see Task::changeStatus().
 *  @see enterToContinue().
 */
void changeStatus() {
    int counter = 1;
    string name;
    bool found = false;

    if (!gTasks.empty()) {
        cout << "\n\tShowing the tasks that you can choose to mark as done:\n";

        for (const auto& val : gTasks) {
            cout << "Task " << counter++ << ": ";
            val->writeData();
            cout << "\n";
        }

        cout << "\n\tWrite the name of the task you want to mark as done (Enter nothing to cancel): ";
        getline(cin, name);

        if (!name.empty()) {
            for (const auto& val : gTasks) {
                if (val->equalName(name)) {

                    if (val->getStatus() == false) {
                        val->changeStatus(Done);

                        found = true;

                        cout << "\n\tThe task's status is set as finished!\n";
                        enterToContinue();
                        break;
                    }
                    else {
                        val->changeStatus(NotDone);

                        found = true;

                        cout << "\n\tReverted the status of the task to unfinished!\n";
                        enterToContinue();
                        break;
                    }
                }
            }
            if (!found) {
                cout << "\n\tNo tasks with that name!\n";
                enterToContinue();
            }
        }
        else {
            cout << "\n\tCanceling mark task as done\n";
            enterToContinue();
        }
    }
    else {
        cout << "\n\tNo tasks in the list to mark as done!\n";
        enterToContinue();
    }
}


/**
 *  Creates a new task.
 *
 *  @see Task::equalName(...).
 *  @see Task::readData().
 *  @see enterToContinue().
 */
void newTask() {
    string name;
    bool dupe = false;
    Task* newTask = nullptr;

    cout << "\n\tWhat is the name of the new task: ";
    getline(cin, name);

    for (const auto& val : gTasks) {
        if (val->equalName(name)) {
            dupe = true;
        }
    }

    if (!dupe) {
        newTask = new Task(name);
        newTask->readData();
        gTasks.push_back(newTask);

        cout << "\n\tSuccesfully added " << name << " into the task list!\n\n";

        enterToContinue();
    }
    else {
        cout << "\n\t" << name << " is already a task in the list!\n";
        enterToContinue();
    }

}


/**
 *  Reads your the task list from a the 'Tasks.dta' file.
 *
 *  @see Task::Task(...).
 */
void readFromFile() {
    int tasks;
    ifstream in("Tasks.dta");
    Task* addTask = nullptr;
    string name;

    if (in) {

        cout << "\nReading from 'Tasks.dta'! ...\n\n";

        in >> tasks;
        in.ignore();

        for (int i = 0; i < tasks; i++) {
            addTask = new Task(in);
            gTasks.push_back(addTask);
        }

        in.close();
    }
    else
        cout << "\nCould not open 'Tasks.dta'!\n\n";
}


/**
 *  Prints out all the tasks to the screen.
 *
 *  @see Task::writeData().
 *  @see enterToContinue().
 */
void showAllTasks() {
    int counter = 1;

    cout << "\n\t" << gTasks.size() << " task/s in the list:\n\n";
    for (const auto& val : gTasks) {
        cout << "Task " << counter++ << ": ";
        val->writeData();
        cout << "\n";
    }
    cout << "\n";

    enterToContinue();
}


/**
 *  Prints all the data about a specific task to screen.
 *
 *  @see Task::equalName(...).
 *  @see Task::writeData().
 *  @see enterToContinue().
 */
void showTask() {
    int counter = 1;
    string taskName;
    bool found = false;

    if (!gTasks.empty()) {
        cout << "\n\tWhat is the tasks name that you want to show: ";
        getline(cin, taskName);

        for (const auto& val : gTasks) {
            if (val->equalName(taskName)) {
                cout << "\nTask " << counter << ": ";
                val->writeData();

                found = true;

                enterToContinue();
                break;
            }
            counter++;
        }
        if (!found) {
            cout << "\n\tNo tasks with that name!\n";
            enterToContinue();
        }
    }
    else {
        cout << "\n\tNo tasks in the list to show!\n";
        enterToContinue();
    }
}


/**
 *  Prints all the tasks with a specific priority to screen.
 *
 *  @see readChar(...).
 *  @see Task::getPriority().
 *  @see Task::writeData().
 *  @see enterToContinue().
 */
void showTasksWithPriority() {
    int counter = 1;
    string taskName;
    char input;
    bool found = false;
    enum Priority prio;

    if (!gTasks.empty()) {

        cout << "\n";
        do {
            input = readChar("\tWhat priority in the task list do you want to see (H)igh, (M)edium or (L)ow");
        } while (input != 'L' && input != 'M' && input != 'H');

        switch (input) {
        case 'L': prio = low;    break;
        case 'M': prio = medium; break;
        case 'H': prio = high;   break;
        }

        for (const auto& val : gTasks) {
            if (val->getPriority() == prio) {
                cout << "\nTask " << counter << ": ";
                val->writeData();

                found = true;
            }
            counter++;
        }
        if (found) {
            enterToContinue();
        }
        else {
            cout << "\n\tNo tasks with that priority!\n";
            enterToContinue();
        }
    }
    else {
        cout << "\n\tNo tasks in the list to show!\n";
        enterToContinue();
    }
}


/**
 *  Clears the terminal and then prints out the command menu for the program.
 */
void writeMenu() {
    system("cls");

    cout << "=================================================================\n"
        << "                       TaskBeDone\n\n"
        << "                    Make your Task Be Done\n"
        << "=================================================================="
        << "\n\n\n"
        << "\tCommands:\n"
        << "\t\tN - Add a new task\n"
        << "\t\tP - See specific task\n"
        << "\t\tS - See all tasks\n"
        << "\t\tR - See all tasks with desired priority\n"
        << "\t\tO - Sort tasks\n"
        << "\t\tV - Move task\n"
        << "\t\tC - Change tasks\n"
        << "\t\tD - Delete task\n"
        << "\t\tM - Changes status (Finished/Unfinished) of task\n"
        << "\t\tQ - Quit\n\n"
        << "\tNOTE: The input for these commands are not case sensitive,"
        << "\n\t      but the input in the commands are case sensitive!\n\n"
        << "=================================================================\n";
}


/**
 *  Prints out command menu in the changeTask function.
 *
 *  @see Task::changeTask().
 */
void writeChangeMenu() {
    cout << "\n\nWhat in the task do you want to change?\n"
        << "\tN - Name of the task\n"
        << "\tD - Description of the task\n"
        << "\tC - Category of the task\n"
        << "\tS - Start date of the task\n"
        << "\tF - Finish date of the task\n"
        << "\tP - Priority of the task\n"
        << "\tQ - Go back to the menu\n\n";
}


/**
 *  Writes the tasks to file.
 *
 *  @see Task::writeToFile(...).
 */
void writeToFile() {
    ofstream outfile("Tasks.dta");

    cout << "\n\tWriting to file 'Tasks.dta'! ...\n";

    if (!gTasks.empty()) {

        outfile << gTasks.size() << '\n';

        for (const auto& val : gTasks)
            val->writeToFile(outfile);
    }
}


/**
 *  Sorts the task list based on a specific entered attribute of the tasks.
 *
 *  @see readChar(...).
 *  @see Task::getName().
 *  @see Task::getCategory().
 *  @see Task::getStartDate().
 *  @see Task::getFinishDate().
 *  @see Task::getPriority().
 *  @see enterToContinue().
 */
void sortTasks() {
    char input;

    sortMenu();

    do {
        input = readChar("\tCategory you want to sort with");
    } while (input != 'N' && input != 'C' && input != 'S' && input != 'F' && input != 'P');

    switch (input) {
    case 'N': gTasks.sort([](Task* it1, Task* it2) { return (it1->getName() < it2->getName()); });             break;
    case 'C': gTasks.sort([](Task* it1, Task* it2) { return (it1->getCategory() < it2->getCategory()); });     break;
    case 'S': gTasks.sort([](Task* it1, Task* it2) { return (it1->getStartDate() < it2->getStartDate()); });   break;
    case 'F': gTasks.sort([](Task* it1, Task* it2) { return (it1->getFinishDate() < it2->getFinishDate()); }); break;
    case 'P': gTasks.sort([](Task* it1, Task* it2) { return (it1->getPriority() < it2->getPriority()); });     break;
    }

    cout << "\n\tThe list has been sorted!\n";
    enterToContinue();
}


/**
 *  Shows the sort command menu for the sortTasks function.
 *
 *  @see sortTasks().
 */
void sortMenu() {
    cout << "\n\tThis is the things you can sort the tasks with:\n"
        << "\t\tN - Name of the task\n"
        << "\t\tC - Category of the task\n"
        << "\t\tS - Start date of the task\n"
        << "\t\tF - Finish date of the task\n"
        << "\t\tP - Priority of the task\n";
}


/**
 *  Swaps/moves the position of two specified tasks in the list.
 *
 *  @see Task::writeData().
 *  @see readInt(...).
 *  @see enterToContinue().
 */
void moveTask() {
    int taskNumber1, taskNumber2, counter = 1;

    if (!gTasks.empty()) {

        for (const auto& val : gTasks) {
            cout << "Task " << counter++ << ": ";
            val->writeData();
            cout << "\n";
        }

        cout << "\n\tWhat task do you want to swap:\n\n";

        taskNumber1 = readInt("\tEnter the task number of the task", 0, gTasks.size());

        if (taskNumber1 > 0) {
            cout << "\n\tNow enter the task number you want to swap the other task with\n\n";

            taskNumber2 = readInt("\tEnter the task number of the task", 0, gTasks.size());

            if (taskNumber2 > 0) {

                auto it1 = gTasks.begin();
                advance(it1, (taskNumber1 - 1));

                auto it2 = gTasks.begin();
                advance(it2, (taskNumber2 - 1));

                swap(*it1, *it2);

                cout << "\n\tTask number " << taskNumber1 << " and " << taskNumber2 << " swapped!\n";
                enterToContinue();
            }
            else {
                cout << "\n\tCanceling moving task!\n";
                enterToContinue();
            }
        }
        else {
            cout << "\n\tCanceling moving task!\n";
            enterToContinue();
        }
    }
    else {
        cout << "\n\tTask list is empty!\n";
        enterToContinue();
    }
}


/**
 *  Makes the user press enter to continue to the main menu.
 */
void enterToContinue() {
    cout << "\n\tPress enter to move on! ...\n\n";
    getchar();
}