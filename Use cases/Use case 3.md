## Use case 3: See all tasks

**Target**

Seeing all the task currently in the list

**Actor**

The user.

**Trigger**

The user wanting to see all the tasks in the list and then enters the 'S' command for seeing all tasks.

**Pre-condition**

1. Having the application open
2. User has to be in the main menu

**Post-condition**

See all the tasks and all the information about every task.

**Main flow**

1. User presses 'S' in the command field to run the see a all task functionality.
2. All the information about all the tasks is displayed

**Exceptions**

2.1 The task list does not contain any tasks

- A message appearing saying the task list does not contain any tasks and gets returned to the main menu.
