## Use case 8: Change status of a task

**Target**

Changes the status of a task. If the task is set as unfinished, it will be set as finished. If it is set as finished, it will be reverted to not finished.

**Actor**

The user.

**Trigger**

The user wanting to change the status of a task in the list and then enters the 'M' command for changing the status of the task in the list.

**Pre-condition**

1. Open the application 
2. User has to be in the main menu
3. There must be at least one task

**Post condition**

Mark the staus of an task to finished

**Main flow**

1. User press 'M' in the command field to run the mark a task as done functionality. 
2. User enters a task name that already exist.
3. If the task is not allready set as finished, the task will be set as finished. If the task is set as finished, it will be reverted to not finished, and returns to main menu after pressing enter 

**Exceptions**

2.1 The user enters a task name that does not exist

- The user gets an error message that task name isn’t valid, and returns to main menu after pressing enter.

3.1 The user enters a task name that doesn’t exist

- Gets an error message that task name isn’t valid, and returns to main menu after pressing enter.
