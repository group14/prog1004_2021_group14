## Use case 7: Delete a specified task

**Target**

Deletes a specified task in the list.

**Actor**

The user.

**Trigger**

The user wanting to delete a task in the list and then enters the 'D' command for deleting the task in the list.

**Pre-condition**

1. Open the application 
2. User has to be in the main menu
3. There must be at least one task

**Post-condition**

A specific task gets deleted from the program.

**Main flow**

1. User press 'D' in the command field to run the delete a task function. 
2. User enters the task name of a task that is in the list that the user wants to delete 
3. User enter 'Y' to confirm the deletion of the task
4. The user gets a message saying the task gets deleted, and returns to the main menu after pressing enter.

**Exceptions**

2.1 User types a task name that does not exist

- Error message that the task does not exist and returns to the main menu.

3.1 The user writes 'N' for no instead of 'Y' for yes

- The user gets a message saying the deletion got canceled since the user said 'N' for no instead of 'Y' for yes, and returns to the main menu.

