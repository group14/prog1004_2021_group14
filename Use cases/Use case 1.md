## Use case 1: Make a new Task

**Target**

Make a new task in the list.

**Actor**

The user.

**Trigger**

The user wanting to make a new task and then enters the 'N' command for adding a new task.

**Pre-condition**

1. Having the application open
2. User has to be in the main menu

**Post-condition** 

New task is added to the system and can be edited by the system.

**Main flow**

1. User press 'N' in command field to run the new task function
2. User enters a name that doesn’t already exist
3. User enters a description for the task
4. User enters a category for the task
5. User enters the start date for the task within given range
6. User enters the finish date for the task within given range
7. User enters one of the three choices for priority for the task
8. The task has now been added to the list, and a message says the new task has been created

**Exception**

1.1 User selects a an already existing name

   - Error message saying name allready exits and then returns to main menu.

5.1 User enters a start date outside the given range (1 - 31 days, 1 - 12 months and 1900 - 2100 years)

   - User have to try again until given date is accepted range.

6.1 User enters a finish date outside the given range (1 - 31 days, 1 - 12 months and 1900 - 2100 years)

   - User have to try again until given date is in accepted range.

7.1 User enters a invalid priority, must be either H for high, M for medium or L for low priority

   - User have to try again until the accepted priority is set.
