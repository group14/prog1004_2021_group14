## Use case 5: Move a specified task

**Target**

Moves/swaps the position of two tasks in the list.

**Actor**

The user.

**Trigger**

The user wanting to move and swap places of two tasks in the list and then enters the 'V' command for swapping the two tasks in the list.

**Pre-condition**

1. Open the application 
2. User has to be in the main menu
3. There must be at least two task

**Post-condition**

Moves and swaps the position of two specified tasks.

**Main flow**

1. User presses 'V' in the command field to run the move task functionality 
2. Displays alle the task with their task number.
3. User enters a valid task number to designate the task to be swapped
4. User enters another valid task number to designate the second task that the first task will swap positions in the list with.
5. The user gets a message saying the task gets deleted, and returns to the main menu after pressing enter.

**Exceptions**

3.1 The use enters the number '0'.

- Message saying that no changes were made and the swap got canceled and returns to the main menu.

3.2 User enters an invalid task number

- Loops until the user enters a valid task number to be swapped or enters '0'.

4.1 The use enters the number '0'.

- Message saying that no changes were made and the swap got canceled and returns to the main menu.

4.2 User enters an invalid task number

- Loops until the user enters a valid task number to be swapped or enters '0'.

4.3 There is only one task in the system

- The task will just swap with it self and nothing will happen.
