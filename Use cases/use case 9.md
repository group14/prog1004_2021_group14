## Use case 9: Shows tasks with a specified priority

**Target**

Shows tasks with a specified priority.

**Actor**

The user.

**Trigger**

The user wanting to see all tasks in the list with a specified priority and then enters the 'R' command for seeing all those tasks with the specified priority.

**Pre-condition**

1. Open the application 
2. User has to be in the main menu
3. There must be at least one task

**Post condition**

Shows all the tasks that has the priority chosen by the user.

**Main Flow**

1. User press 'R' in the command field to run the show tasks with priority function
2. User chooses the priority they want see the tasks with
3. Shows all the tasks data with the specified priority, and then returns to the main menu after pressing enter.

**Exceptions**

2.1 User chooses an invalid priority

- Loops until the user types in a priority that is valid.

3.1 There is no task with selected priority.

- Gives a message that there is no task with selected priority, and returns to the main menu.
