## Use case 4: Sort all tasks based on a characteristic of the task

**Target**

Sorts all tasks in the list based on a characteristic of the tasks.

**Actor**

The user.

**Trigger**

The user wanting to sort the tasks based on the tasks characteristic and then enters the 'O' command for sorting the task list based on that characteristic.

**Pre-condition**

1. Having the application open 
2. User has to be in the main menu
3. There must be at least two tasks in the list


**Post-condition**

Sorts all the task list based on a desired characteristic the user chooses.

**Main flow**

1. User presses 'O' in the command field to run the sort of task function.
2. User enters the letter to the characteristic they want to sort the tasks with. The user can sort the tasks based on either:
    - Name (alphabetically)
    - Category (alphabetically)
    - Start date (descending)
    - Finish date (descending)
    - Priority (from highest to lowest)

**Exceptions**

2.1 User chooses a characteristic that is not valid

 - Loops until the input is valid.

