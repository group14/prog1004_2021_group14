## Use case 2: See a specific task

**Target**

See a specific task in the task list.

**Actor**

The user.

**Trigger**

The user wanting to see a specific task in the list, then enters the 'P' command to do see that specific task.

**Pre-condition**

1. Having the application open
2. User has to be in the main menu

**Post-condition**

Being able to see the specific task and all the information related to that task.

**Main flow**

1. User presses 'P' in the command field to run the see a specific task function
2. User enters the name of a task that exists in the list to see that task
3. All the information about the entered the task is displayed in the terminal

**Exceptions**

2.1 User enters task name that doesn’t exist

- User gets an error message that the task doesn’t exist and returns to main menu.
