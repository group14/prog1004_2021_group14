## Use case 6: Change a specified task

**Target**

Change a specific characteristic of a specific task.

**Actor**

The user.

**Trigger**

The user wanting to change a specific characteristic of a specific task and then enters the 'C' command for changing that characteristic in the task.

**Pre-condition**

1. Having the application open
2. User has to be in the main menu
3. There must be at least one task in the list

**Post-condition**

Changes a specific characteristic about a specific task in the list.

**Main flow**

1. User press 'C' in the command field to run the change task functionality
2. User enters the name of the task they want to change
3. User chooses the characteristic they want to change about the task
4. The user inputs the new data about the chosen characteristic they want to change. These are the options:
    - Name
    - Description
    - Category 
    - Start date 
    - Finish date
    - Priority

5. Asks the user if there is anything else they want to change with the task or type 'Q' to quit back to the main menu

**Exceptions**

2.1 User types the name of a task that does not exist

- The user gets a error message saying the task does not exist and returns to the main menu.

3.1 User types a invalid command when choosing a characteristic to change

- Loops until the user types valid input.

4.1 The user types an invalid date or an invalid priority

- Loops until the user enters valid values for date and valid characters for priority.
